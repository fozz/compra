def eliminar_elementos_repetidos(lista):
    return list(set(lista))
# set lista que no admite duplicados

def main():
    habitual = ("patatas", "leche", "pan")

    especifica = []

    while True:
        elemento = input("Elemento a comprar: ")
        if elemento == "":
            break
        especifica.append(elemento)
        # Añade elememtos a la lista


    compra_total = eliminar_elementos_repetidos(list(habitual) + especifica)
    elementos_habituales = len(habitual)
    elementos_especificos = len(especifica)
    elementos_totales = len(compra_total)

    print("\nLista de la compra:")
    for item in compra_total:
        print(item)

    print("Elementos habituales:", elementos_habituales)
    print("Elementos específicos:", elementos_especificos)
    print("Elementos en lista:", elementos_totales)

if __name__ == "__main__":
    main()

